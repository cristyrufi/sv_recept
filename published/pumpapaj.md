# Pumpapaj

## Pajdeg
 - 5 dl 100% fullkornsdinkelmjöl
 - 2 dl tahini (sesampasta)
 - 1,5 dl vatten
 - en nypa salt

Sätt på ugnen på 200° C på varmluft-funktionen.

Blanda alla ingredienser i en bunke och tryck den ner i en ugnssäker form.

Med en gaffel gör små hål på botten så luften försvinner och kan andas genom.

Grädda i ugnen i ca 15 minuter.

## Ostsås till fyllning
 - 5 dl vatten
 - 1 dl solroskärnor
 - 0,5 dl kokosflingor
 - 1 msk paprikapulver
 - 1 msk fiberhusk
 - 2 tsk örtsalt Herbamare

Blanda alla ingredienser i en mixer tills konsistensen blir tjock.


## Fyllning
 - ca 1 kg färsk och urkärnade matpumpa
 - 0,5 dl vatten
 - 2 st lökar
 - 2 st vitlöksklyftor

Tärna pumpan och koka tillsammans med vattnet i en rymlig gryta tills den blir mjuk.

Hacka löken och pressa vitlöken.

Blanda i grytan ostsåsen och tillsätt löken och vitlöken.

Sätt på ugnen på 200° C i varmt luft funktion.

Häll i pajformen den blandade pumpan med ostsåsen och grädda i ca 30 minuter.

Låt det svalna lite innan servering.
