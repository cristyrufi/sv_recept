# Lasagne

 - 5 dl blötlagda sojabitar
 - 450 g bladspenat
 - 800 g krossade tomater
 - 2 st lökar
 - 2 st vitlöksklyftor
 - 400 g tofu (tärnade)
 - 1 msk salt
 - 1,5 msk Herbamare örtsalt
 - 1 msk honung
 - 1,5 dl tomatpuré
 - 3 dl vatten
 - 20 fullkornslasagneplattor
 - 1 dl solroskärnor
 - en sats av ostsås (se receptet nedan)

> Förberedelse:
 - Blötlägga sojabitar några timmar innan
 - Tillaga ostsåsen

Sätt ugnen på 200° C

Hacka lök och pressa vitlöken, häll det i en gryta och steka ca en minut. Tillsätt de andra ingredienser utom de sista 3 och låt det koka ca 3 minuter.

I en djup långpanna ca 39x47 cm, häll i 3 dl av den röda såsen först innan du lägger pastan. Halvera resten av den röda och den vita såsen (ostsås). Varva i följande ordning 10 fullkornslasagneplattor, röd sås och ostsås i 2 lager. Garnera med solroskärnor.

Grädda i mitten av ugnen ca 30 minuter.

> Detta blir ca 5 portioner och den går bra att frysas.
