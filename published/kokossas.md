# Söt kokossås

 - 1,5 dl kokosflingor
 - 0,5 dl fullkornshavregryn
 - 0,5 dl honung
 - 2 dl vatten
 - en nypa salt

Koka havregrynen med 1 dl vatten till en gröt med salt i. Låt det svalna.

I en mixer eller kaffekvarn krossa kokosflingorna till en mjöl.

I en skål häll 1 dl vatten och den krossade kokosflingorna och låt dem ligga i blöt några minuter.

När havregrynsgröten är sval, lägg det i en mixer tillsammans med de andra ingredienser och mixa till en slätt konsistens ca 1 eller 2 minuter. Klart att servera eller lägg den i kylen några timmar för att ha en tjockare konsistens innan servering.

Detta blir ca 3 dl sås.
