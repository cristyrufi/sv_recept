# Bröd med kärnor och nötter

## Smet
 - 7 dl vatten
 - 25 g jäst
 - 10 dl fullkornsmjöl (dinkel, grahamns eller en blanding av de två)
 - 0,5 dl honung
 - 1 tsk salt

## Fyllning
 - 2 dl pumpakärnor
 - 2 dl solroskärnor
 - 2 dl hasselnötter, mandlar eller valnötter
 - 1 dl russin
 - 1 dl gojibär eller torkade fikon
 - 4 tsk fiberhusk


> Blir 2 limpor.

Häll i mjölet, honungen och saltet i en stor bunke (3 liters)

Värm upp vattnet tillsammans med jästen till 38° C under omrörning i en kastrull.

Häll vattnet med jästen i bunken med mjölet och använd en hand för att röra ihop allt till en fin smet.

Sätt på en duk över bunken och ställ den på ett varmt ställe, såsom vid ett element som är varmt etc, i ca 30-60 min då den kladdiga degen har stigit tills ungefär kanten av 3 litersbunken.

Sätt ugnen på 175° C på varmluft-funktionen.

Ta bort luften genom att röra degen/smeten. Häll i kärnor och nötter och blanda, sedan häll fiberhusk och blanda väl igen. Klä två brödformar eller två små ungsäkra formar med bakplåtspapper och fördela degen mellan dem.

Grädda alla tillsammans i ugnen i 175° C på varmluft-funktionen ca 45 minuter.

> För att testa om brödet är klart så ta en kniv och försiktigt tryck ner i mitten av brödet. Om det kommer deg med upp så är det inte klart ännu.

Ta ut ur ugnen och låt svalna.

Lägg på dukar på brödet och låt stå 48-72 timmar. (Detta görs så att jästen ska helt försvinna ifrån degen och därmed inte skada kroppen etc)

Skär upp i önskad tjocklek som passar dina behov och frys in eller ät upp snart!

> Brödet går utmärkt att frysa in och sedan rosta eller värma i ugnen när den skall ätas.

> "Och Jesus sa till dem: Jag är livets bröd. Den som kommer till mig ska aldrig hungra, och den som tror på mig ska aldrig törsta." Johannesevangeliet 6:35.
