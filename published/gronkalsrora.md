# Grönkålsröra

 - 225 g färsk eller fryst grönkål
 - 2 st vitlöksklyftor
 - 1 st lök
 - 1 dl solroskärnor
 - 2 dl tomatpuré
 - 2 tsk örtsalt Herbamare
 - lite honung

Hacka grönsakerna och häll de i en gryta. Sätt grytan på spisen med låg/medel-värme och lägg locket på. Efter ca en minut börja röra blandningen då och då. Det är viktigt att ha locket på efter att du är klar med att röra om. Under tiden krossa solroskärnorna och mät de andra ingredienser. Grönkålen skall krympa och när stamen är mjukt nog häll i rester av ingredienser och den är klart att serveras.

> Detta blir 6 dl.

> Gott som pålägg eller fyllning till taco tortillas, piroger, etc.

> Sojabitar funkar väldigt bra att ha i.
