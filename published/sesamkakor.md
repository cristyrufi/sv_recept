# Sesamkakor

 - 1,5 dl sesamfrön
 - 1,5 dl cashewnötter
 - 8 dl fullkornshavregryn
 - 1 tsk salt
 - 1 tsk kardemumma
 - 1,5 dl fast honung
 - 1,2 dl vatten

Ta fram en rimligt stor bunke för att få plats med alla ingredienserna.

Finkrossa sesamfrön, cashewnötterna och havregrynen en i taget och häll dem i bunken.

Blanda vattnet och honungen väl tillsammans och sedan häll dem med de andra ingredienserna.

Tillsätt saltet och kardemumman och rör ihop till en kladdig deg.

Dela degen i två delar och sträck ut den med hjälp av en brödkavel på en skärbräda. 

Sätt på ugnen på 150° C på varmluft-funktionen.

Med en rund kakform dela små kakor. Baka kakorna i ca 25 min eller tills de får lite brun färg.

Detta blir mellan 40 och 60 st kakor beroende på hur stora man gör dem.
