# Moussaka
 - 1 aubergin (stor)
 - 225 g fryst bladspenat
 - 400 g krossade tomater
 - 1 lök
 - 2 vitlöksklyftor
 - 0,5 tsk salt
 - 1 msk örtsalt Herbamare
 - 1 tsk honung
 - 0,5 dl tomatpuré
 - 3 dl ostsås (se receptet nedan)

 ## Förberedelse
 - Tillaga ostsåsen

## Tillagning
Sätt ugnen på 175° C på varmluft-funktionen

Hacka löken och pressa vitlöken och häll dem i en gryta, tillsätt de andra ingredienser utom ostsåsen, och låt det koka några minuter tills spenaten har tinat upp.

Skiva auberginen i tunna skivor.

I en ugnssäker form 28x22 cm eller liknande, häll i 2 dl av den röda såsen sedan lägg de skivade aubergin och sist häll resten av såsen. Garnera med ostsås.


Grädda i mitten av ugnen ca 35 minuter.
