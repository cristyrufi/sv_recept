# Paté

 - 2 dl solroskärnor
 - 2 dl vatten
 - 2 dl 100% fullkornsmjöl
 - 1 dl tahini
 - 1 dl kokosmjölk
 - 250 g potatis
 - 100 g morötter
 - 100 g lök
 - 2 st vitlöksklyftor
 - 2 tsk salt

Sätt på ugnen på 175° C på varmluft-funktionen

Hacka grönsakerna, häll dem i en mixer, tillsätt resten av ingredienserna och mixa väl till en fin smet.

Häll smeten i en liten ugnssäker form och grädda i ugnen i ca 40 minuter.

Låt det svalna innan servering.

> Den går bra att frysas.
