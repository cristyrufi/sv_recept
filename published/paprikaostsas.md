# Paprika-ostsås

 - 1 dl hirs
 - 9 dl vatten
 - 160 g röd paprika
 - 0,5 dl kokosflingor
 - 1 dl mandlar
 - 1 dl solroskärnor
 - 1 tsk vitlökspulver
 - 1 tsk salt
 - 1 msk Herbamare örtsalt

 > Detta blir ca 1250 ml

Skölj hirsen i omväxlande varmt och kallt vatten eller gnugga hirsen väl med kallt vatten flera gånger tills vattnet inte längre blir smutsigt.

I en kastrull koka upp de första 3 ingredienser på en låg värme tills alla gryn blir öppnade.

Blanda i en mixer den kokta hirsen och alla andra ingredienser och mixa väl till en fin smet.

> Det går att blanda i tofu eller blötlagda och kokta sojabitar.

> Det passar utmärkt till nachochips + dina favoritgrönsaker.
