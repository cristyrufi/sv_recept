# American Pizza

## Dough
 - 5 dl 100% fullkornsdinkelmjöl
 - 2 dl tahini (sesampasta)
 - 1,5 dl vatten
 - en nypa salt

Blanda alla ingredienserna i en bunke och tryck ner den i en bakplåt.

Med en gaffel gör små hål på botten så luften försvinner och den kan andas genom dem.

Sätt på ugnen på 200° C på varmluft-funktionen och lägg bakplåten i den kalla ugnen och låt den bakas under 15 minuter.


## "Ost"
 - 5 dl vatten
 - 1 dl solroskärnor
 - 0,5 dl kokosflingor
 - 1 msk fiberhusk
 - 2 tsk örtsalt Herbamare eller salt

Blanda alla ingredienser i en mixer tills konsistensen blir slät och lite tjock.


## Toppingförslag
 - 2,5 dl torr sojafärs
 - 3 dl krossade tomater
 - salt efter smak
 - timjan och oregano efter smak
 - lök
 - paprika

Blötlägg sojafärsen under minst 2 timmar och koka efteråt.

Blanda i en skål de krossade tomaterna, salt och kryddor.

Hacka paprikan och löken och lägg dem i olika skålar.

På den förbakade degen sprida ut tomatsåsen och toppa med de andra ingredienser och tillslut häll osten på. Strö lite timjan och oregano på pizzan för att få lite extra smak.

Baka i ugnen ca 20 minuter på 200° C på varmluft-funktionen.
