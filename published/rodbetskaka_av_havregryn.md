# Rödbetskaka av havregryn

 - 100 g tärnade rödbetor
 - 4 dl vatten
 - 4 dl fullkornshavregryn
 - 0,5 dl honung
 - 0,5 dl kokosflingor
 - 4 msk carob
 - 2 tsk fiberhusk
 - en nypa salt

Sätt ugnen på 175° C på varmluft-funktionen.

Blanda alla ingredienser i en mixer till en fin smet. Det tar mellan 1 och 2 minuter.

Grädda i ugnen i 55 minuter.

> Servera gärna med kall carobsås (se recept nedan)
