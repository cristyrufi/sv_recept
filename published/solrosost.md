# Solrosost

 - 1 dl hirs
 - 5 dl vatten
 - 2 tsk örtsalt Herbamare eller vanligt salt
 - 1 dl solroskärnor
 - 1 dl kokosflingor eller 35 g kokosnöt

> Detta blir 700 ml

Skölj hirsen i omväxlande varmt och kallt vatten eller gnugga hirsen väl med kalt vatten flera gånger tills vattnet inte längre blir smutsigt.

I en kastrull koka upp hirsen och vattnet på en låg värme tills alla gryn blir öppnade.

Blanda i en mixer solroskärnor och kokosflingor/nöt tills finmald, tillsätt den kokta hirsen och örtsalt och mixa väl till en fin smet.
