# Bananpannkaka

 - 4 st mogna bananer
 - 2 msk bovete
 - en nypa salt

Sätt på ugnen på 200° C på varmluft-funktionen.

I en bunke mosa bananerna för hand.

Krossa bovete och häll det i de mosade bananer tillsammans med saltet och blanda väl.

Forma små pannkakor i en långpanna med bakplåtspapper och grädda i ugnen i ca 12 minuter.
