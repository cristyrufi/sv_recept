# Frukt- och nötstek

 - 3 dl blötlagda kikärtor (eller ca 1 dl torra)
 - 2 dl hirs
 - 1 dl kokosflingor
 - 10 dl vatten (4 dl till frukten och 6 dl till bakningen)
 - 340 g skalad och tärnad ananas
 - 2,5 dl torkade fikon
 - 2,5 dl torkade aprikos
 - 1 dl mandlar
 - 1 tsk kardemumma
 - 1 tsk salt

## Förberedelser

Blötlägg kikärtorna

Blötlägg i en skål fikon, aprikos och ananas med 4 dl vatten och lägg den i kylen

## Tillagning

Sätt på ugnen på 200° C på över- undervärme

Skölj de blötlagda kikärtor och fin krossa efteråt i en matberedare.

Gnugga och skölj hirsen väl.

Ta fram de blötlagda frukter från kylen.

Förbered 6 dl vatten och kokosflingor.

Krossa mandlarna.

I en bunke blanda alla ingredienser utom de krossade mandlarna.

I en ugnssäker form ca 28x22 cm eller liknande häll blandningen och garnera med de krossade mandlar.

Baka på 200° C i ca 50 minuter.

Servera varmt eller kallt med carobsås, kokosgrädde eller kokosglass.

> Den går bra att frysa in.
