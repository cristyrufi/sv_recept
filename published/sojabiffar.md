# Sojabiffar

 - 7,5 dl sojabitar/färs
 - 2 dl solroskärnor
 - 1 dl tomatpuré
 - 1 st lök
 - 1 st paprika
 - 2 st vitlöksklyftor
 - 1,5 msk psyllium
 - 0,5 tsk salt
 - 2 tsk örtsalt Herbamare
 - ev. hackad persilja eller koriander efter smak

## Förberedelser
> Blötlägg sojabitarna ca 4 timmar.

Ta bort vattnet ifrån de blötlagda sojabitarna och skölj med rent vatten. Koka sedan upp dem ca 5 minuter med tillräckligt vatten så att de täcker sojabitarna.

I en matberedare krossa de kokta sojabitarna och lägg dem i en bunke. 

Krossa solroskärnorna och sedan hacka löken och paprikan, pressa vitlöken och lägg dem i samma bunke. 

Tillsätt rester av ingredienser och blanda ihop för hand till en deg-konsistens.

Sätt på ugnen på 175° C på varmlufts-funktionen.

Forma biffar och lägg dem på en bakplåt med bakplåtspapper.

Grädda i ugnen i 20-25 minuter.

Låt biffarna svalna först innan du tar dem från bakplåten.

Detta blir ca 12 st biffar med 12 cm diameter.

> De går bra att frysa in.
