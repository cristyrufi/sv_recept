# Bovetepudding

 - 1,5 dl bovete
 - 200 g dadelpasta
 - 1 dl carob
 - 7 dl vatten
 - en nypa salt

Skölj bovetet.

I en kastrull koka alla ingredienser på en låg temperatur tills den blir som en gröt.

När gröten är klar blanda det väl i en mixer. Låt svalna innan servering.

> Detta blir en liter.
