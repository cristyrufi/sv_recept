# American Pizza

## Röd sås

 - 1,5 dl pumpakärnor
 - 1 dl sesamfrö
 - 3 st medium stora lökar
 - 400 g krossade tomater
 - 1,5 tsk paprika pulver
 - 2 tsk örtsalt Herbamare
 - 0.5 tsk salt
 - 4 dl vatten

Rosta pumpakärnor och sesamfrö tillsammans

Rosta lök separat.

I en mixer häll de krossade tomater, vatten, de rostade kärnor och fröer och lökar och mixa till en slätt konsistens.

I en kastrull häll såsen och krydda med rester av ingredienser och låt det kokas på spisen på mediun temperatur i ca 5 mimuter till den blir lite tjock. Ha locket på under kok tiden.

Detta blir ca 1,5 liter sås.

## Vitsås

 - 2,5 dl råris eller 3 dl rårismjöl
 - 0,5 dl majsmjöl
 - 5 dl vatten
 - 0,5 dl kokosmjölk
 - 0,5 dl tahini
 - 2 tsk örtsalt herbamare
 - 1 tsk salt
 - 1 tsk lökpulver

Har du råris fintkrossa det.

Häll alla ingredienser i en rymlig kastrull och låt det kokas i omrörning tills den får en tjock konsistens.

Detta blir ca 1 liter.

I en ungform varva vitsås efteråt rödsås och vitsås igen och avsluta med rödsås på.
