# Zucchinisoppa

 - 1 kg tärnade zucchini
 - 1 dl vatten
 - 1,5 dl cashewnötter
 - 2 tsk salt
 - 1 tsk vitlökspulver

Koka zucchini tillsammans med vattnet i låg temperatur tills den blir mjuk.

I en mixer blanda de kokta zucchinin med rester av ingredienser till en slät tjock konsistens.

 > Detta blir 1250 ml.
