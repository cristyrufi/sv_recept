# Carobsås

 - 1 dl vatten
 - 0,5 dl carob
 - 0,5 dl honung
 - 1 dl tahini

Blanda väl i en burk först vattnet och caroben, sedan tillsätt honungen och blanda väl, sist tillsätt tahinin och blanda väl.

 > Blanda aldrig alla ingredienser på en gång, det blir inget bra.

 > Detta blir ca 3,5 dl av carobsås.
