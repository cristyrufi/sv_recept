# Bananpudding

> Obs: Bilden visar annanasvarianten.

 - 1 dl hirs
 - 3 st mogna bananer
 - en nypa salt
 - 5 dl vatten
 - 1 dl cashewnötter
 - 1 dl riven kokos eller 35 g kokosnöt

> Detta blir en liter
> Det går också bra att byta ut bananer mot annanas, i så fall använd 245 g annanas.

Skölj hirsen väl i omväxlande varmt och kallt vatten eller gnugga hirsen väl med vatten flera gånger tills vattnet inte längre blir smutsigt.

I en kastrull häll de första 4 ingredienser och koka upp, med locket på, i låg värme tills alla gryn blir öppnade.

När hirsen är kokt blanda i en mixer med rester av ingredienser.

Låt det svalna innan servering. Toppa med dina favoritnötter, fröer och bär.

> Tips: Efter den har svalnat kan man lägga den i kylen tills den blir riktigt kall och god!
