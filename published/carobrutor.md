# Carobrutor

 - 1,5 dl carob
 - 1,5 dl vatten
 - 1,5 dl honung
 - 0,5 dl tahini
 - 8 dl fullkornshavregryn
 - en nypa salt
 - kokosflingor till garnering

I en bunke blanda alla ingredienserna till en kladdig deg.

I en form ca 18x27 cm eller liknande, strö lite kokosflingor på formen sedan tryck ner degen och platta till en jämn yta. Strö lite kokosflingor på toppen och tryck ner för att få dem att bli fasta.

Dela i 5 rader på den breda delen och 4 på den smala för att göra rutorna.

Förvaras bäst i frysen och ta ut vid servering.
