# Majonnäs

 - 3 dl vatten
 - 1,5 dl cashewnötter
 - 1 dl kokosnöt eller kokosflingor
 - 0,5 dl solroskärnor
 - 1,5 tsk salt
 - 2 msk citronsaft

I en skål blötlägg nötter och fröer i vatten i ca 8 timmar, sedan kör det i en mixer till en fin smet, lägg till saltet och citronsaften. Klart att servera.

> Detta blir 5 dl.
