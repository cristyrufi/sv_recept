# Grädde

 - 250 ml kokosgrädde
 - 1 msk honung (fast)

> Vill du ha en sötare grädde tillsätt 1 msk honung.

Blanda kokosgrädde i en liten skål tillsammans med honungen och lägg den i kylen.

> Om du lägger den i frysen så stelnar den fortare, men glöm inte att ta ut den innan den blir helt frusen!
