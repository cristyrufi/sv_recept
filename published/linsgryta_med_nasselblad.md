# Linsgryta med nässelblad

 - 2,5 dl torra gröna linser
 - 125 g färska nässelblad (plocka och rensa med handskar)
 - 200 ml kokosmjölk
 - 1 tsk örtsalt Herbamare
 - 1 tsk salt
 - 1 lök
 - 2 vitlöksklyftor
 - 3 dl vatten

> Blötlägg linserna ca 6 timmar.

Plocka, rensa och väg nässlorna först och skölj efteråt. Om du väger de när bladen är blöta så väger de mer och måtten kommer inte att stämma. 

> Skölj nässlorna noga, ta bort eventuella rötter och grova delar. Använd handskar när du arbetar med nässlor. 
 
> Om du glömde väga bladen när dem var torra, så borde de kanske väga ca 215 g rena och klara att hacka, men det beror på hur mycket vatten du har kvar.

Hacka nässelbladen och lägg dem i en bunke.

Ta bort vattnet ifrån de blötlagda linserna och skölj dem med rent vatten.

I en gryta lägg linserna, häll kokosmjölken och vattnet och tillsätt de hackade nässlorna och låt det kokas på mellan-värme på spisen med locket på.

Hacka löken och pressa vitlöksklyftorna och lägg dem i grytan. Tillsätt salt och låt det kokas med locket på i ca 30 minuter eller tills linserna är kokta. Börja räkna tiden sedan du satt grytan på spisen.

> Detta blir ca 1,3 liter. 
