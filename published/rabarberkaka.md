# Rabarberkaka

## Fyllning
 - 430 g rabarber
 - 200 g färska dadlar
 - 1,5 dl vatten

I en kastrull koka dadlarna och vatten och mosa väl. 

Skala rabarberna och skär i skivor. Blanda sedan rabarberna i dadelmosen.

## Smet
 - 3 dl 100% fullkornsdinkelmjöl
 - 3 dl varmt vatten
 - 0,5 dl honung
 - 2 tsk fiberhusk
 - 0,5 tsk salt

Sätt på ugnen på 175° C på varmluft-funktionen.

I en skål blanda vattnet och honung. Sedan blanda de torra ingredienser en i taget till en fin smet.

I en ca 1,5 liter ugnssäker form häll hälften av smeten, sedan lägg hälften av rabarberröran på smeten. Tillslut häll resten av smeten ovanpå det och resten av rabarberröran på toppen.

Grädda kakan i mitten av ugnen i ca 30 minuter.
