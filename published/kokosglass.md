# Kokosglass

 - 5 dl kokosgrädde eller kokosmjölk (500 ml)
 - 1 dl mandlar
 - 1/2 dl carob
 - 1/2 dl honung

Finkrossa mandlarna i matberedare och sedan tillsätt resten av ingredienser och blanda väl. 

> Det går bra att blanda för hand också om du vill.
