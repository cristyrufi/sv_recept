# Granola

 - 15 dl fullkornshavregryn
 - 1,5 dl mandlar eller hasselnötter
 - 1,5 dl pumpakärnor
 - 1,5 dl solroskärnor
 - 1 dl sesamfrön
 - 1 dl kokosflingor
 - en nypa salt
 - 2 dl varmt vatten
 - 1,5 dl honung
 - 1 msk mald kardemumma

Hacka nötter i önskad storlek, för hand eller i matberedare.

I en skål blanda vattnet och honungen.

Sätt ugnen på 125° C på varmluft-funktionen

I en bunke tillsätt alla ingredienser och blanda väl för hand, lägg en duk på bunken och vänta ca 5 minuter (syftet är att låta det bli lite torrt) blanda igen och separera de som har klibbat ihop sig.

Baka i ugnen i 3 långa pannor tills de får lite brun färg, ca 55 minuter. Efter de första 25 minuter börja att röra och sedan efter varje 15 minuter därefter.
