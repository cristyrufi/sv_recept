# Ostsås

 - 600 g kokt potatis (mjöliga)
 - 3 dl vatten
 - 1 dl kokosmjölk
 - 1,5 dl cashewnötter
 - 1,5 tsk salt
 - 1 tsk Herbamare örtsalt

> Såsen blir 1250 ml.

> Vill du göra hälften av såsen, använd 300 g potatis, 2 dl vatten, 1 dl cashewnötter, 1 tsk herbamare örtsalt och 1 dl kokosmjölk.

Mixa alla ingredienser i en mixer till en slät tjock konsistens.

> Använd smoothe funktion på mixern om din masking har det, annars kör den långsamt först och öka hastigheten så småningom.
