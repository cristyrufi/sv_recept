# Paté

## Ingredients in Cups

 - 1 cup sunflower seeds, baked or toasted
 - 1.5 cup water
 - 1 cup whole wheat flour
 - 3 tbs coconut flakes
 - 250 g potatoes
 - 100 g carrots
 - 1 onion
 - 2 cloves of garlic
 - 1 tbs Herbal salt (Herbamare)


## Ingredients in Deciliters 

 - 2 dl sunflower seeds, baked or toasted 
 - 3.5 dl water
 - 2 dl whole wheat flour
 - 0.5 dl coconut flakes
 - 250 g potatoes
 - 100 g carrots
 - 1 onion
 - 2 cloves of garlic
 - 1 tbs Herbal salt (Herbamare)


## Instructions

1. Preheat the oven to 175° C (350° F) in fan-forced mode

1. Wash and cut the vegetables in small pieces leaving the potato and carrot skins on.
 
1. Pour all the ingredients into a blender except the whole wheat flour, and mix well to a fine batter.

1. In a bowl pure the batter from the blender and add the whole wheat flour and mix well with a whisk.

1. Pour the batter into a small oven-proof dish or a loaf pan and bake it in the middle of the oven for about 35 minutes. If you are using a loaf pan you should have a baking paper sheet to remove the paté without difficulties.

1. The paté is done when a table knife inserted in the middle comes out clean.
 
1. Allow to cool before cutting and serving.

> Difficulty: Easy

> It can be frozen.

> Yield: ca 1 liter (around 2 pints) batter.

> Prep. Time: 20 min.

> Baking time: 30-35 min.

> Cooling time: ca 15 min.

> Gluten free: Replace the whole wheat flour for chickpea flour with the same amount.  

> Serving suggestion: Paté with green salad, home made ketchup, potatoes salad and boiled beets.

> Serving amount: 4 servings or 2 servings if the Paté is served as the only main dish lets say with green salad and ketchup. 
