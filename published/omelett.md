# Omelett

 - 5 dl blötlagda sojabönor (andvänd 2 dl torra sojabönor)
 - 4 st tomater
 - 4 dl vatten
 - 2 st gullök
 - 2 st vitlöksklyftor (pressade)
 - 1 st paprika
 - 2 msk tahini (sesampasta)
 - 1 msk fiberhusk
 - 1 msk citronsaft
 - basilikablad efter smak
 - 2 tsk salt

Sätt ugnen på 175° C på varmluft-funktionen

Hacka grönsakerna

Blanda i en mixer bönorna, vattnet och fiberhusk till en fin smet.

I en bunke häll smeten och tillsätt resten av ingredienserna. Blanda väl.

 > Vill du använda omeletten som pålägg häll blandningen i en långpanna och grädda i ugnen i ca 50 minuter. Placera plåten i den näst understa platsen i ugnen.

 > Vill du ha den tjockare använd en ugnsform och grädda i ugnen i ca 50 minuter. När man gräddar den på detta sättet så går den bra att frysa in.

 > Det gar bra att byta ut tomater mot andra grönsaker som vitkål, blomkål, broccolli, morötter etc. om man gör det i den tjockare varianten.
