# Bröd

 - 20 dl mjöl (100% fulkornsdinkel eller grahamsmjöl)
 - 14 dl vatten
 - 1 dl honung
 - 1 tsk salt (celtic sea salt)
 - 50 g Kronjäst för bröd
 - ev. 1 dl fröer och/eller nötter (såsom sesamfrön, solroskärnor, valnötter)

> Blir 3 ungsplåtar, 1 cm tjock. Som kan skäras i t.ex. 16 bitar vardera, som blir 48 bitar totalt.

Häll i mjölet, honungen och saltet i en stor bunke (6 liters)

> Om du vill så kan du lägga i fröer och nötter tillsammans med mjölet, eller så kan du vänta tills du häller ut smeten på plåtarna.

Värm upp vattnet tillsammans med jästen till 38° C under omrörning.

Häll vattnet med jästen i bunken med mjölet och använd en hand för att röra ihop allt till en fin smet.

Sätt på en duk över bunken och ställ den på ett varmt ställe, såsom vid ett element som är varmt etc, i ca 30-60 min då degen har stigit tills ungefär kanten av en 6 litersbunke.

Häll ut degen/smeten jämnt fördelat på tre ungsplåtar med bakplåtspapper på.

> Om du vill ha fröer eller nötter i så kan du strö på dem nu och sprida ut lite mer mjöl för att kunna trycka ner dem utan att fastna (ca 0,5 dl mjöl per plåt om det behövs)

Grädda alla tillsammans i ugnen ca 15-20 minuter på 175° C på varmluft-funktionen.

> För att testa om brödet är klart så ta en kniv och försiktigt tryck ner i mitten av brödet. Om det kommer deg med upp så är det inte klart ännu.

Ta ut ur ugnen och låt svalna.

Lägg på dukar på brödet och låt stå 48-72 timmar. (Detta görs så att jästen ska helt försvinna ifrån degen och därmed inte skada kroppen etc)

Skär upp i former som passar dina behov och frys in eller ät upp snart!

> För att enkelt få ut brödet, ta hela papret med brödet och vänd upp och ner över skärbrädan. Ta sedan bort papret från brödet.

> Brödet går utmärkt att frysa in och sedan rosta eller värma i ugnen när den skall ätas.

> "Och Jesus sa till dem: Jag är livets bröd. Den som kommer till mig ska aldrig hungra, och den som tror på mig ska aldrig törsta." Johannesevangeliet 6:35.
