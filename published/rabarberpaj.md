# Rabarberpaj

## Fyllning
 - 430 g rabarber
 - 200 g urkärnade dadlar
 - 1,5 dl vatten

## Pajdeg
 - 4 dl fullkornsmjöl (grahams eller dinkel)
 - 1/2 dl pumpakärnor
 - 1/2 dl sesamfrön
 - 1,5 dl vatten
 - en nypa salt

Sätt ugnen på 200° C på varmluft-funktionen.

Rosta pumpakärnor och sesamfrön och finkrossa efter.

> Alternativt kan du bara finkrossa kärnor och fröer utan att rosta.

Blanda alla pajdegsingredienser i en bunke till en deg.

Tryck ut degen i en liten ugnssäker form (28x18 cm eller liknande) så att den hänger ut lite över kanterna.

I en kastrull koka dadlar med vattnet och mosa till en gröt.

Skala rabarber och skär i skivor.

Blanda ner rabarber i dadelgröten.

Lägg rabarberblandningen på pajdegen och grädda pajen mitt i ugnen 25-30 minuter.

Servera pajen med grädde (se nedan).
