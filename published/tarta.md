# Tårta

## Soja-botten
 - 2 dl torra sojabönor eller 5 dl blötlagda
 - 3 dl vatten
 - 1 dl fast honung
 - 1 msk citronsaft
 - en nypa salt
 - en nypa kardemumma

## Kikärtskräm
 - 2 dl torra kikärtor eller 5 dl blötlagda
 - 8 dl vatten
 - 1 dl fast honung
 - en nypa salt

## Garnering
 - 2,5 dl kokosgrädde (helst Garant eller Santa Maria)
 - 0,5 dl fast honung
 - Eventuellt färska jordgubbar, blåbär eller annan sorts bär.

> Förberedelser:

> Blötlägg sojabönor och kikärtor i olika skålar i ca 8 timmar.

> Blanda kokosgrädde och honung i en skål och ställ den i kylen. Ju längre tid i kylen desto bättre.

Sätt på ugnen på 175° C på varmluft-funktionen.

Ta bort vattnet ifrån de blötlagda sojabönorna och skölj med rent vatten. I en mixer häll sojabönor, vatten och honung och kör till en fin smet, 1 eller 2 minuter. Tillsätt citronsaft, salt och kardemumma och kör lite till för att blanda det väl.

Häll smeten i en ugnssäker form ca 3 liter eller liknande och grädda i ugnen i 30 minuter.

Efter 20 minuter eller när soja-bottnen är gräddat kör kikärtorna, vatten och honung i en mixer i 1 eller 2 minuter. Konsistensen måste vara som mjölk. När det blir skum så är det klart. Häll mjölken i en lagom stor gryta, sätt på spisen och rör om och om igen tills skummet försvinner. Detta tar mellan 10 och 15 minuter beroende på temperaturen på spisen. När det börjar koka använd locket för att skydda dina händer och ansikte (det kan komma små droppar av den blivande kikärtskrämen). Tillsätt saltet på slutet. Häll kikärtskrämen på soja-bottnen och låt det svalna.

När tårtan är lagom kall, ställ den i kylen. Innan servering garnera med kokosgrädde och frukt efter smak. Det går också bra att tillaga tårtan och ta den tillbaka till kylen för att få en bättre konsistens innan servering.
