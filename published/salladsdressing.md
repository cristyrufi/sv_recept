# Salladsdressing

 - 2 dl tahini (sesampasta)
 - 2 dl varmt vatten
 - 0,5 dl tomatpuré
 - 2 tsk salt

I en rymlig skål blanda för hand alla ingredienser till en fin konsistens.

 > Detta blir ca 5 dl.
