# Ugnsbakad havregrynsgröt

 - 5 dl fullkornshavregryn
 - 5 dl vatten
 - 2 dl kokosmjölk
 - 1,5 dl russin
 - 3 mogna bananer
 - en nypa salt

Sätt ugnen på 175° C.

Skiva bananer i 1,5 cm skivor. I en bunke blanda alla ingredienser och häll blandningen i en liten ugnsäker form och grädda i ca 50 minuter.
