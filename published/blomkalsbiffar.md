# Blomkålsbiffar

 - ca 600 g blomkål
 - 2 dl hasselnötter
 - 1 st lök
 - 1 vitlöksklyfta
 - 1 msk fiberhusk
 - 2 tsk örtsalt Herbamare

Rensa blomkålen och löken och skär dem i små delar.

Finhacka i en matberedare den okokta blomkålen eller finhacka det för hand.

Finhacka löken.

Finkrossa hasselnötterna.

Sätt på ugnen på 150° C på över- och undervärme på varmluft-funktionen.

Pressa vitlöken.

I en bunke lägg alla ingredienser och blanda väl för hand till en deg, forma biffar och lägg dem på en bakplåt med bakplåtspapper.

Grädda i ugnen i 25 minuter.

Låt biffarna svalna först innan du tar dem från bakplåten.

> Konsistensen är ganska mjuk så om du har rester och du vill värma dem gör så här: lägg dem separat, inte en på den andra annars klibbar de ihop sig.

Detta blir 12 st biffar med 12 cm diameter.
