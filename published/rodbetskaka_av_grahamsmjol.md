# Rödbetskaka av grahamsmjöl

 - 100 g tärnade rödbetor
 - 4 dl vatten
 - 3 dl fullkornsgrahamsmjöl
 - 0,5 dl honung
 - 0,5 dl kokosflingor
 - 4 msk carob
 - 2 tsk fiberhusk
 - en nypa salt

Sätt ugnen på 175° C på varmluft-funktionen.

Blanda alla ingredienser i en mixer till en fin smet. Det tar mellan 1 och 2 minuter.

Grädda i ugnen i 40 minuter.

> Servera gärna med kall carobsås (se recept nedan)
