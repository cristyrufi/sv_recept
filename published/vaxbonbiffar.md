# Vaxbönbiffar

 - 500 g vaxbönor
 - 0,5 dl cashewnötter eller sesamfrön
 - 1,5 dl pumpakärnor
 - 1 st röd paprika
 - 2 st lökar
 - 2 st vitlöksklyftor
 - 2 tsk örtsalt Herbamare
 - 1 msk fiberhusk

Kör i matberedaren de okokta vaxbönor, lök och paprika tills de är fint krossade.

Finkrossa nötter och kärnorna.

I en bunke blanda de fint krossade ingredienser, pressa vitlöksklyftor och häll den i och tillsätt saltet och fiberhusk.

Sätt på ugnen på 150° C på varmluft-funktionen


Forma biffar efter önskad storlek. Det blir ca 10 st av 12 cm diameter.

Grädda biffarna i ugnen i 35 minuter.
