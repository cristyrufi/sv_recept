# Taco-tortilla

 - 4 dl 100% fullkornsdinkelmjöl
 - 1 dl sojamjöl
 - 1 msk fiberhusk
 - 1 tsk salt
 - 2,5 dl vatten

Sätt på ugnen på över- och undervärme i 225° C med en tom ugnsplåt i.

Blanda väl alla ingredienserna i en bunke. När den är välblandad dela den i fyra delar och kavla lagom tunn.

När ugnen har nåt 225° C börja baka en i taget. När 1,5 minut har gått vänd tortillan och ta den ur ugnen när det har gått max 2,5 minuter för båda sidorna sammanlagt. (om de är där längre så blir de hårda).

Detta blir 4 st taco-tortillas av ca 24 cm diameter eller 8 st av ca 12 cm diameter.
