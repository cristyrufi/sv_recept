# Ostkaka

## Botten

 - 2 dl hasselnötter
 - 2,5 dl 100% fullkornsströbröd
 - 2 msk linfrön
 - 1 dl vatten

## Fyllning

 - 4 dl blötlagda sojabönor (andvänd 1,5 dl torra sojabönor)
 - 4 dl kokosmjölk (400 ml)
 - 1,5 dl honung
 - en nypa salt
 - 1,5 dl 100% fullkornsmjöl
 - 2 msk limejuice

Sätt på ugnen på 200° C

Botten: Krossa hasellnötterna och linfrön. I en bunke blanda alla ingredienser väl. Tryck degen ner i en liten ugnssäker form (28x18 cm eller liknande, den behöver inte vara med löstagbara sidor) och ha det i kylen medan du gör fyllningen.

Fyllningen: I en mixer kör alla fyllningsingredienserna förutom limejuice till en fin konsistens, tillsätt limejuice och blanda väl. Bred ut fyllningen över botten och grädda i ca 40 minuter.

 > Gör ditt egna ströbröd genom at rosta fullkornsbröd och krossa efteråt.
