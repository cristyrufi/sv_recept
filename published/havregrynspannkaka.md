## Havregrynspannkaka

 - 7 dl vatten
 - 5 dl fullkornshavregryn
 - 0,5 dl honung
 - 1 dl kokosflingor
 - 1 tsk salt
 - ev. 0,5 tsk mald kardemumma

Sätt på ugnen på 200° C på varmluft-funktionen.

När ugnen har nått temperaturen (200° C); i en mixer häll alla ingredienser och mixa till en slätt konsistens.

Häll smeten på en bakplåt med bakplåtspapper och gör den jämn. Täck hela papperet. Grädda mitt i ugnen mellan 8 och 10 minuter.

Låt pannkakan svalna innan du delar den.

Detta blir 4 st av ca 21x17 cm eller 8 st av ca 11x8 cm.

> Du kan även göra en rulltårta om du låter bli att skära den och istället fylla den med önskad sylt och rulla den.
