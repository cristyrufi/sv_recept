#!/usr/bin/env python3
from setuptools import setup


setup(
        name='recipe_generator',
        version='1.1',
        packages=[],
        entry_points={
            'console_scripts':[
                'recipe_generator=mdtohtml:run',
                ],
            },
        install_requires=[
            'markdown2',
            ],
        )
