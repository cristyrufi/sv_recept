import subprocess
import glob, os


def run():
    os.chdir("/recept/print")
    generation_performed = False
    for file in glob.glob("*.md"):
        (output_filename_without_extension, extension) = os.path.splitext(file)
        output_extension = '.html'
        output_folder = 'html/'
        output_file = output_folder + output_filename_without_extension + output_extension
        if not os.path.exists(output_file):
            with open(output_file, 'w') as f:
                html_content = '<style>'
                html_content += subprocess.run(['cat', output_folder + 'github.css'], capture_output=True, text=True).stdout
                html_content += '</style>'
                html_content += subprocess.run(['markdown2', file], capture_output=True, text=True).stdout
                f.write(html_content)
                print(f'Generated {output_file}')
                generation_performed = True
    if not generation_performed:
        print('No generation was performed. This probably means that all files already have an equivalent html files.')

if __name__ == '__main__':
    run()
